<!-- vim: set sw=2 et sts=2: -->
  <appendix id="_appendix">
    <title>Appendix</title>
    <para>Here are backgrounds of this document.</para>
    <section id="_the_debian_maze">
      <title>The Debian maze</title>
      <para>The Linux system is a very powerful computing platform for a networked computer.  However, learning how to use all its capabilities is not easy. Setting up the LPR printer queue with a non-PostScript printer was a good example of stumble points. (There are no issues anymore since newer installations use the new CUPS system.)</para>
      <para>There is a complete, detailed map called the "SOURCE CODE".  This is very accurate but very hard to understand.  There are also references called HOWTO and mini-HOWTO.  They are easier to understand but tend to give too much detail and lose the big picture.  I sometimes have a problem finding the right section in a long HOWTO when I need a few commands to invoke.</para>
      <para>I hope this "Debian Reference (version @-@dr-version@-@)" (@-@build-date@-@) provides a good starting direction for people in the Debian maze.</para>
    </section>
    <section id="_copyright_history">
      <title>Copyright history</title>
      <para>The Debian Reference was initiated by me, Osamu Aoki &lt;osamu at debian dot org&gt;, as a personal system administration memo. Many contents came from the knowledge I gained from <ulink url="http://lists.debian.org/debian-user/">the debian-user mailing list</ulink> and other Debian resources.</para>
      <para>Following a suggestion from Josip Rodin, who was very active with the <ulink url="https://www.debian.org/doc/ddp">Debian Documentation Project (DDP)</ulink>, "Debian Reference (version 1, 2001-2007)" was created as a part of DDP documents.</para>
      <para>After 6 years, I realized that the original "Debian Reference (version 1)" was outdated and started to rewrite many contents.  New "Debian Reference (version 2)" is released in 2008.</para>
      <para>I have updated "Debian Reference (version 2)" to address new topics (Systemd, Wayland, IMAP, PipeWire, Linux kernel 5.10) and removed outdated topics (SysV init, CVS, Subversion, SSH protocol 1, Linux kernels before 2.5).  References to @-@release-outdated@-@ release situation or older are mostly removed.</para>
      <para>This "Debian Reference (version @-@dr-version@-@)" (@-@build-date@-@) covers mostly @-@Codename-doc-1@-@ and @-@Codename-doc-2@-@ Debian releases.</para>
      <para>The tutorial contents can trace its origin and its inspiration in followings.</para>
      <itemizedlist>
        <listitem>
          <para> "<ulink url="http://www.ibiblio.org/pub/Linux/docs/linux-doc-project/users-guide/user-beta-1.pdf.gz">Linux User's Guide</ulink>" by Larry Greenfield (December 1996) </para>
          <itemizedlist>
            <listitem> <para> obsoleted by "Debian Tutorial" </para> </listitem>
          </itemizedlist>
        </listitem>
        <listitem>
          <para> "<ulink url="https://www.debian.org/doc/manuals/debian-tutorial/">Debian Tutorial</ulink>" by Havoc Pennington. (11 December, 1998) </para>
          <itemizedlist>
            <listitem> <para> partially written by Oliver Elphick, Ole Tetlie, James Treacy, Craig Sawyer, and Ivan E. Moore II </para> </listitem>
            <listitem> <para> obsoleted by "Debian GNU/Linux: Guide to Installation and Usage" </para> </listitem>
          </itemizedlist>
        </listitem>
        <listitem>
          <para> "<ulink url="http://archive.debian.net/woody/debian-guide">Debian GNU/Linux: Guide to Installation and Usage</ulink>" by John Goerzen and Ossama Othman (1999) </para>
          <itemizedlist>
            <listitem> <para> obsoleted by "Debian Reference (version 1)" </para> </listitem>
          </itemizedlist>
        </listitem>
      </itemizedlist>
      <para>The package and archive description can trace some of their origin and their inspiration in following.</para>
      <itemizedlist>
        <listitem> <para> "<ulink url="https://www.debian.org/doc/manuals/debian-faq/">Debian FAQ</ulink>" (March 2002 version, when this was maintained by Josip Rodin) </para> </listitem>
      </itemizedlist>
      <para>The other contents can trace some of their origin and their inspiration in following.</para>
      <itemizedlist>
        <listitem>
          <para> "<ulink url="http://packages.debian.org/search?keywords=debian-reference&amp;searchon=sourcenames&amp;exact=1&amp;suite=all&amp;section=all">Debian Reference</ulink> (version 1)" by Osamu Aoki (2001–2007) </para>
          <itemizedlist>
            <listitem> <para> obsoleted by the newer "Debian Reference (version 2)" in 2008.  </para> </listitem>
          </itemizedlist>
        </listitem>
      </itemizedlist>
      <para>The previous "Debian Reference (version 1)" was created with many contributors.</para>
      <itemizedlist>
        <listitem> <para> the major contents contribution on network configuration topics by Thomas Hood </para> </listitem>
        <listitem> <para> significant contents contribution on X and VCS related topics by Brian Nelson </para> </listitem>
        <listitem> <para> the help on the build scripts and many content corrections by Jens Seidel </para> </listitem>
        <listitem> <para> extensive proofreading by David Sewell </para> </listitem>
        <listitem> <para> many contributions by the translators, contributors, and bug reporters </para> </listitem>
      </itemizedlist>
      <para>Many manual pages and info pages on the Debian system as well as upstream web pages and <ulink url="https://www.wikipedia.org/">Wikipedia</ulink> documents were used as the primary references to write this document.  To the extent Osamu Aoki considered within the <ulink url="https://en.wikipedia.org/wiki/Fair_use">fair use</ulink>, many parts of them, especially command definitions, were used as phrase pieces after careful editorial efforts to fit them into the style and the objective of this document.</para>
      <para>The gdb debugger description was expanded using <ulink url="http://wiki.debian.org/HowToGetABacktrace">Debian wiki contents on backtrace</ulink> with consent by Ari Pollak, Loïc Minier, and Dafydd Harries.</para>
      <para>Contents of the current "Debian Reference (version @-@dr-version@-@)" (@-@build-date@-@) are mostly my own work except as mentioned above.  These has been updated by the contributors too.</para>
      <para>The author, Osamu Aoki, thanks all those who helped make this document possible.</para>
    </section>
    <section id="_document_format">
      <title>Document format</title>
      <para>The source of the English original document is currently written in <ulink url="https://en.wikipedia.org/wiki/DocBook">DocBook</ulink> XML files.  This Docbook XML source are converted to HTML, plain text, PostScript, and PDF. (Some formats may be skipped for distribution.)</para>
    </section>
  </appendix>
